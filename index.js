
// forEach function
function forEach(array, callbackToRunOnEachItem) {
    for (let index = 0; index < array.length; index++) {
    const currentValue = array[index]
    callbackToRunOnEachItem(currentValue, index, array)
    }
}

const letters = ['a', 'b', 'c', 'd', 'e']
    forEach(letters, function (letter, index, array) {
    const letterIsVowel = 
    letter === 'a' || letter === 'e' || letter === 'i' || letter === 'o' || letter === 'u'
    const letterType = letterIsVowel ? 'vowel' : 'consonant'
    console.log(`The letter '${letter}' at position ${index} of ${array} is a ${letterType}.`)
})

// map function
function map(array, callBackToMap) {
    for(let index = 0; index < array.length; index++) {
    const currentValue = array[index];
    callBackToMap(currentValue, index, array);
    }
}

let originalArray = [2, 3, 4, 5];
let newArray = [];
map(originalArray, function (number, index, array) {
    newArray[index] = originalArray[index] * 2;
    console.log(newArray);
})

// some function
function some(array, callSomeLetters) {
    for( let index = 0; index < array.length; index++) {
    const currentValue = array[index];
    callSomeLetters(currentValue, index, array);
    }
}


let someLetters = ['a', 'b', 'c', 'd', 'e']
some(someLetters, function(letter, index, array) {
const letterIsTrue = letter === 'a' || letter === 'b' || letter === 'c';
const letterIsFalse = letterIsTrue ? 'true' : 'false';
console.log(letterIsFalse)
})

// find function
function find(array, callSomeNumbers) {
    for(let index = 0; index < array.length; index++) {
    const currentValue = array[index];
    callSomeNumbers(currentValue, index, array);
    }
}

let originalNumbers = [1, 2, 3, 4, 5, 6];
let newOriginalNumbers = [];
find(originalNumbers, function(number, index, array) {
    newOriginalNumbers[index] = originalNumbers[index] > 3;
    console.log(newOriginalNumbers);
})

// findIndex function
function findIndex(array, callFirstLargestNumber) {
    for(let index = 0; index < array.length; index++) {
    const currentValue = array[index];
    callFirstLargestNumber(currentValue, index, array);
    }
}

let setOfNumbers = [5, 12, 8, 130, 44];
let newSetofNumbers = [];
findIndex(setOfNumbers, function(number, index, array) {
    newSetofNumbers[index] = setOfNumbers[index] > 13;
    console.log(newSetofNumbers);
})

// every function
function every(array, callAllNumbersUnderForty) {
    for(let index = 0; index < array.length; index++) {
    const currentValue = array[index];
    callAllNumbersUnderForty(currentValue, index, array);
    }
}

let allNumbers = [1, 20, 39, 29, 10, 13];
let newAllNumbers = [];
every(allNumbers, function(number, index, array) {
    newAllNumbers[index] = allNumbers[index] < 40;
    console.log(newAllNumbers);
})

// filter function
function filter(array, callAllAgesOverEighteen) {
    for(let index = 0; index < array.length; index++) {
    const currentValue = array[index];
    callAllAgesOverEighteen(currentValue, index, array);
    }
}

let ages = [32, 33, 16, 40];
let newAges = [];
filter(ages, function(age, index, array) {
    newAges[index] = ages[index] > 18;
    console.log(newAges);
})



















